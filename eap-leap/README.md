EAP-LEAP
========

You're kidding?

LEAP was invented by Cisco. It was not adopted as an 802.11 standard.
Perhaps rightfully so ... because due to lack of peer-review, it contained serious flaws.

Today even Cisco write "LEAP ... is considered fully compromised".

See https://www.cisco.com/c/en/us/td/docs/wireless/controller/technotes/8-6/b_Cisco_Wireless_LAN_Controller_Configuration_Best_Practices.html#concept_B60A5AB4E17448348C440DAF76C395BA

