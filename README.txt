What's in it?
=============

This is just a bunch of matching configuration files for wpa_supplicant and
hostapd.



Setting up the demoCA
=====================

The Makefile create a demo certification authority. This is used to create
self-signed certification for TLS and TTLS.  Just run "make" to set it all
up.



Creating an access-point
========================

Run


	hostapd hostapd.conf

in one of the subdirectories makes an access-point out of your laptop.

Note that my sample-setup doesn't need a radius-server such as freeRadius.
Newer versions of hostapd have an embedded radius-server that I'm using.
But I'm one of those people that compile hostapd from source (git hash
hostap-1-bp-1052-gbb726c1).  If you only have the hostapd from your
distribution, then good luck to you.



Connecting to the access-point
==============================

Run

	wpa_supplicant -i wlan1 -c wpa_supplicant.conf

in one of same subdirectory as above to connect to the access point.



Checking the connection
=======================

Run

	iw wlan1 link

or

	wpa_cli status

The results of that commands are saved in the "result.txt" files in
the various subdirectories.
