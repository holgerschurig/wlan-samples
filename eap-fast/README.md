Notes on the fast-pac.txt file
==============================

While I committed the contents of this file into the GIT repository.
But to get EAP-FAST running, this is actually necessary.

Instead, you can create a file that is writable by wpa_suppliant and
that is totally empty. wpa_supplicant will then populate the file with
the needed information by itself.
