all: demoCA/server_cert.pem demoCA/client1_cert.pem demoCA/cacert.der demoCA/dh.pem
.NOTPARALLEL:: all

# Web pages with infos and tips
#
#  http://www.bndlg.de/kursangebote/aglinux/index.php/Eap-tls
#
# -batch -subj to prevent questions
#	http://www.tu-harburg.de/rzt/it-sicherheit/tuhh-dfnpki/beantragen.html


#################################################################
#
#  Create client 1 keys
#

# sign client1 key
.NOTPARALLEL:: demoCA/client1_cert.pem
demoCA/client1_cert.pem: demoCA/private/client1_key.pem  demoCA/client1_req.pem
	openssl ca \
		-batch \
		-passin pass:democa \
		-policy policy_anything \
		-out demoCA/client1_cert.pem \
		-extensions xpclient_ext \
		-extfile demoCA/xpextensions \
		-infiles demoCA/client1_req.pem

# create client1 key
.NOTPARALLEL:: demoCA/private/client1_key.pem
demoCA/private/client1_key.pem demoCA/client1_req.pem:
	openssl req \
		-batch \
		-subj "/C=DE/ST=BY/L=Germering/O=Snakeoil GmbH/OU=Linux Client1 Demo CERT/CN=localhost" \
		-new \
		-keyout demoCA/private/client1_key.pem \
		-passout pass:client1 \
		-out demoCA/client1_req.pem \
		-days 730




#################################################################
#
#  Create server keys
#

# sign server keys
.NOTPARALLEL:: demoCA/server_cert.pem
demoCA/server_cert.pem: demoCA/private/cakey.pem demoCA/cacert.pem demoCA/server_req.pem
	openssl ca \
		-batch \
		-passin pass:democa \
		-policy policy_anything \
		-out demoCA/server_cert.pem \
		-extensions xpserver_ext \
		-extfile demoCA/xpextensions \
		-infiles demoCA/server_req.pem

# create server keys
.NOTPARALLEL:: demoCA/private/server_key.pem demoCA/server_req.pem
demoCA/private/server_key.pem demoCA/server_req.pem:
	openssl req \
		-batch \
		-subj "/C=DE/ST=BY/L=Germering/O=Snakeoil GmbH/OU=Linux Server Demo CERT/CN=laptop" \
		-new \
		-nodes \
		-keyout demoCA/private/server_key.pem \
		-passout pass:server \
		-out demoCA/server_req.pem \
		-days 730


#################################################################
#
#  Create Certificate Authority
#  (similar to CA.sh -newca)
#

# convert CA key to DER format
.NOTPARALLEL:: demoCA/cacert.der
demoCA/cacert.der: demoCA/cacert.pem
	openssl x509 \
		-in demoCA/cacert.pem \
		-out demoCA/cacert.der \
		-outform der

# sign CA key
.NOTPARALLEL:: demoCA/cacert.pem
demoCA/cacert.pem: demoCA/careq.pem demoCA/private/cakey.pem
	openssl ca \
		-batch \
		-passin pass:democa \
		-create_serial \
		-out demoCA/cacert.pem \
		-days 1095 \
		-batch \
		-keyfile demoCA/private/cakey.pem \
		-selfsign \
		-extensions v3_ca \
		-infiles demoCA/careq.pem \


# create CA key
.NOTPARALLEL:: demoCA/private/cakey.pem demoCA/careq.pem
demoCA/private/cakey.pem demoCA/careq.pem: demoCA/prep.stamp
	openssl req \
		-subj "/C=DE/ST=BY/L=Germering/O=Snakeoil GmbH/OU=Linux Demo CA/CN=localhost.localnet" \
		-new \
		-keyout demoCA/private/cakey.pem \
		-out demoCA/careq.pem \
		-passout pass:democa



#################################################################
#
#   Create Diffie-Hellman group for EAP-FAST forward secrecy
#
.NOTPARALLEL:: demoCA/dh.pem
demoCA/dh.pem: demoCA/prep.stamp
	openssl dhparam -out demoCA/dh.pem 2048



#################################################################

prep: demoCA/prep.stamp
demoCA/prep.stamp:
	mkdir -p demoCA/certs
	mkdir -p demoCA/crl
	mkdir -p demoCA/newcerts
	mkdir -p demoCA/private
	touch demoCA/index.txt
	touch $@


clean:
	rm -rf demoCA/certs
	rm -rf demoCA/crl
	rm -rf demoCA/index.txt
	rm -rf demoCA/newcerts
	rm -rf demoCA/private
	rm -f demoCA/*.stamp
	rm -f demoCA/*.pem
	rm -f demoCA/*.der
	rm -f demoCA/*.attr
	rm -f demoCA/*.old
	rm -f demoCA/serial

